// const testObject = {Moe: "Moses", Larry: "Louis", Curly: "Jerome"};
// console.log(testObject);

function invert(obj) {
    if(!obj || Array.isArray(obj)) return [];
   let invertObject = {};
   for(let key in obj)
   {
       let objKey = key;
       let objValue = obj[objKey];

       invertObject[objValue] = key;
   }

//    console.log(invertObject);
      return invertObject;
}

// invert(testObject);
module.exports = invert;
