function values(obj){
    if(!obj || Array.isArray(obj)) return [];
    let valueArr = [];
    for (let key in obj)
    {
        let value = obj[key];
        let type = typeof(value);
        if(type !== 'function')
        {
            // console.log(type);
            valueArr.push(value);
        }
        
        
    }

    // console.log(valueArr);
    return valueArr;
}

// values(testObject);
module.exports = values;
