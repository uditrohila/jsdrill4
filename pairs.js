function pairs(obj) {
    if(!obj || Array.isArray(obj)) return [];
    let keyValue = [];
    for(let key in obj)
    {   let pair = [];
        let value = obj[key];
        pair.push(key);
        pair.push(value);
        keyValue.push(pair);

    }

    // console.log(keyValue);
    return keyValue;
}

// pairs(testObject);
module.exports = pairs;

