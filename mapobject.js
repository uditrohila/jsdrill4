function mapObject(obj, cb) {
    if(!obj || !cb) return [];
    for(let key in obj)
    {
        let values = obj[key];
        let transformproperty = cb(values);
        obj[key] = transformproperty;
    

    }
    
    return obj;

}

// function cb(value)
// {
//    return value + 5;
// }

// mapObject(testObject,cb);
// console.log(testObject);


module.exports = mapObject;


// console.log(testObject);