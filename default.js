function defaults(object, defaultProps){
    if(!object || !defaultProps) return [];
    for(let property in defaultProps){
        if(object[property] === undefined){
            object[property] = defaultProps[property];
        }
    }
    // console.log(object);
    return object
}

// defaults(testObject,defaultProps);
module.exports = defaults;