
//Retrieve all the names of the object's properties. 
// Return the keys as strings in an array.

function Keys(obj){
    let strArray = [];
    if(!obj || Array.isArray(obj)) return [];
    
    for(let key in obj){
       let  keys = key;
        // console.log(key);
        strArray.push(keys);
    }
    
    // console.log(result);
    return strArray;
}

// Keys(testObject);
module.exports = Keys;